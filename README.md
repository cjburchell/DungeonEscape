# Dungeon Escape

An updated version of my older gaming project (DE15). A game inspired by the Dragon Quest/Final Fantasy games from Super Nintendo. 

Download Latest Version [Here](https://gitlab.com/cjburchell/DungeonEscape/-/jobs/4012464295/artifacts/download)

## TODO
### Features
 - Escort Quests
 - Kill monster quests
 - Create More Quests
 - Improved inventory window
 
 ### Improvements
 - Balance pass
 

 